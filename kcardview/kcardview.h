/***************************************************************************
                          kcardview.h  -  description
                             -------------------
    begin                : Fri Jul  6 09:00:44 BST 2001
    copyright            : (C) 2001 by Richard Wareham
    email                : richwareham@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef KCARDVIEW_H
#define KCARDVIEW_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <kapp.h>
#include <qwidget.h>
#include "main_win.h"

#include <kaboutapplication.h>

#include "cardhelper.h"

/** KCardView is the base class of the porject */
class KCardView : public MainWindow
{
  Q_OBJECT
public:
  /** construtor */
  KCardView(QWidget* parent=0, const char *name=0);
  /** destructor */
  ~KCardView();
  /** No descriptions */
  void setReader(QString reader);
protected: // Protected atributes
  KAboutApplication *about;
  CardHelper helper;
public slots: // Public slots
  /** No descriptions */
  void slotExit();
  /** No descriptions */
  void slotAbout();
  /** No descriptions */
  void slotView();
protected: // Protected methods
  /** No descriptions */
  QString formattedHex(QString hex);
};

#endif
