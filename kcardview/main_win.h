/****************************************************************************
** Form interface generated from reading ui file './main_win.ui'
**
** Created: Fri Jul 6 13:06:29 2001
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <qvariant.h>
#include <qwidget.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QPushButton;
class QTextView;

class MainWindow : public QWidget
{ 
    Q_OBJECT

public:
    MainWindow( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~MainWindow();

    QPushButton* PushButton1;
    QPushButton* PushButton1_2;
    QPushButton* PushButton1_2_2;
    QTextView* text_box;

public slots:
    virtual void slotView();
    virtual void slotAbout();
    virtual void slotExit();

protected:
    QVBoxLayout* MainWindowLayout;
    QHBoxLayout* Layout1;
    bool event( QEvent* );
};

#endif // MAINWINDOW_H
