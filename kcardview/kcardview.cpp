/***************************************************************************
                          kcardview.cpp  -  description
                             -------------------
    begin                : Fri Jul  6 09:00:44 BST 2001
    copyright            : (C) 2001 by Richard Wareham
    email                : richwareham@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "kcardview.h"
#include <kapp.h>

#include <qtextview.h>

KCardView::KCardView(QWidget *parent, const char *name) : MainWindow(parent, name)
{
  about = new KAboutApplication(this, "About", false);
  text_box->setText("");
}

KCardView::~KCardView()
{
  delete about;
}

/** No descriptions */
void KCardView::slotExit()
{
	this->close();
}

/** No descriptions */
void KCardView::slotAbout()
{
  about->show();
}

#define dump_file(id, desc) \
  { text_box->append("<br>Reading " desc); \
  str = ""; \
  helper.beginTransaction(); \
  helper.selectFile(id, header); \
  text_box->append("Apdu returned " + helper.APDUReturnCode()); \
  text_box->append(helper.describeHeader(header)); \
  text_box->append("File contents"); \
  helper.readFile(str); \
  helper.endTransaction(); \
  text_box->append(formattedHex(str)); \
  text_box->append("Done"); \
  KApplication::kApplication()->processEvents(); \
  }


/** No descriptions */
void KCardView::slotView(){
  long rv;
  QByteArray header;

  text_box->setText("");
  text_box->append("Connecting... ");
  rv = helper.connect();
  if(rv != SCARD_S_SUCCESS) { return; }

  text_box->append("Readers list:");
  text_box->append(helper.readers().join("<br>"));
  text_box->append("End of readers list.");

  text_box->append("<br>Selected reader: " + helper.reader());

  text_box->append("<br>Connecting to card.");
  rv = helper.connectCard();
  if(rv != SCARD_S_SUCCESS) { return; }
  KApplication::kApplication()->processEvents();

  text_box->append("Getting card ATR");
  text_box->append(formattedHex(helper.ATR()));
  text_box->append("done<br>");
  KApplication::kApplication()->processEvents();
  QString str;

  dump_file(QString("fd 01"), "card information");
  dump_file(QString("fd 02"), "dir11");
  dump_file(QString("fd 03"), "summary");
  dump_file(QString("fd 04"), "main certificate");
  dump_file(QString("fd 05"), "RFU16");
  dump_file(QString("fd 06"), "RFU32");
  dump_file(QString("fd 07"), "RFU64");

  text_box->append("<br>Disconnecting from card.");
  rv = helper.disconnectCard();
  if(rv != SCARD_S_SUCCESS) { return; }
  KApplication::kApplication()->processEvents();

  text_box->append("<br>Disconnecting");
  helper.disconnect();
  if(rv != SCARD_S_SUCCESS) { return; }

  text_box->append("Finished<br>");
}

/** No descriptions */
QString KCardView::formattedHex(QString hex){
  unsigned int line_off, offset;
  QString str;

  line_off = 0;
  while(line_off < hex.length()) {
    for(offset = line_off; offset < line_off+(8*3); offset+=3) {
      if(offset < hex.length()) {
        str = str + hex.mid(offset, 2) + " ";
      } else {
        str = str + "00 ";
      }
    }
    for(offset = line_off; offset < line_off+(8*3); offset+=3) {
      if(offset < hex.length()) {
        BYTE b = hex.mid(offset, 3).toUInt(0, 16);
        if(b >= 32 && b <= 118) {
          str = str + QString(QChar(b));
        } else {
          str = str + ".";
        }
      } else {
        str = str + ".";
      }
    }
    line_off += 8*3;
    if(line_off < hex.length()) { str = str + "<br>"; }
  }

  return str;
}
