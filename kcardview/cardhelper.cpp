/***************************************************************************
                          cardhelper.cpp  -  description
                             -------------------
    begin                : Fri Jul 6 2001
    copyright            : (C) 2001 by Richard Wareham
    email                : richwareham@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "cardhelper.h"

// Uncomment to print debugging information.
// #define DEBUG_CARDVIEW

#ifdef DEBUG_CARDVIEW
#include <iostream.h>
#define DEBUG_MSG(msg) cout << msg;
#else
#define DEBUG_MSG(msg)
#endif

CardHelper::CardHelper()
{
  m_isConnected = false;
  m_cardPresent = false;
  m_reader.truncate(0);
  m_apduReturn.resize(2);

  m_readers.clear();
}

CardHelper::~CardHelper()
{
  if(m_isConnected) { disconnect(); }
}

long CardHelper::connect(QString host)
{
  if(m_isConnected) { return SCARD_S_SUCCESS; }

  long rv;

  if(host.isEmpty()) {
    DEBUG_MSG("Connecting to local PC/SC middleware" << endl);
    rv = SCardEstablishContext( SCARD_SCOPE_SYSTEM, NULL, NULL, &hContext );
  } else {
    DEBUG_MSG("Trying to connect to >" << host << "<" << endl);
    rv = SCardEstablishContext( SCARD_SCOPE_GLOBAL, host.latin1(), NULL, &hContext );
  }

  if(rv != SCARD_S_SUCCESS) { return rv; }

  m_isConnected = true; // Yay!

  DEBUG_MSG("Getting reader list" << endl);
  // Get list of readers.
  DWORD dwReaders = 0;
  rv = SCardListReaders( hContext, 0, 0, &dwReaders );
  if(rv != SCARD_S_SUCCESS) { disconnect(); return rv; }
  if( dwReaders == 0 ) { return -1; }

  char *mszReaders = new char[dwReaders];
  rv = SCardListReaders( hContext, 0, mszReaders, &dwReaders );
  if(rv != SCARD_S_SUCCESS) { disconnect(); delete mszReaders; return rv; }

  m_readers.clear();
  for ( unsigned int i=0; i < dwReaders - 1; i++ ) {
    m_readers.append(&mszReaders[i]);
    while ( mszReaders[++i] != 0 );
  }
  if(m_readers.count() == 0) { return -1; }
  m_reader = m_readers[0];

  return SCARD_S_SUCCESS;
}

long CardHelper::disconnect()
{
  if(!m_isConnected) {
    return SCARD_S_SUCCESS; // No point disconnecting when not connected.
  }

  DEBUG_MSG("Disconnecting" << endl);

  long rv = SCardReleaseContext( hContext );

  if(rv != SCARD_S_SUCCESS) { return rv; }

  m_isConnected = false;
  m_readers.clear();
  m_reader.truncate(0);

  return SCARD_S_SUCCESS;
}

/** No descriptions */
QStringList CardHelper::readers(){
  return m_readers;
}

/** No descriptions */
void CardHelper::setReader(QString reader){
  m_reader = reader;
}

/** No descriptions */
QString CardHelper::reader(){
  return m_reader;
}

/** No descriptions */
long CardHelper::connectCard(){
  long rv;
  SCARD_READERSTATE_A rgReaderStates[1];

  // Check for card.
  rgReaderStates[0].dwCurrentState = SCARD_STATE_EMPTY;
  rv = SCardGetStatusChange( hContext, 0, rgReaderStates, 1 );
  if((rgReaderStates[0].dwEventState & SCARD_STATE_EMPTY) == 1) {
    // No card.
    return -1;
  }

  if(!m_cardPresent) {
    // Connect
    m_cardPresent = true;
    rv = SCardConnect(hContext, reader(),
		      SCARD_SHARE_SHARED, 
		      SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
		      &hCard, &m_prefProtocol);
    if(rv != SCARD_S_SUCCESS) { return rv; }
  } else {
    rv = SCardReconnect(hCard, SCARD_SHARE_SHARED, 
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
                        SCARD_RESET_CARD, &m_prefProtocol);
    if(rv != SCARD_S_SUCCESS) { return rv; }
  }

  /* Get ATR, etc. */
  unsigned long dwReaderLen=50, dwState, dwProt, dwAtrLen=255;
  char pcReaders[50];
  unsigned char pbAtr[255];
  rv = SCardStatus( hCard, pcReaders, &dwReaderLen, &dwState, &dwProt,
            		    pbAtr, &dwAtrLen );
  if(rv != SCARD_S_SUCCESS) { return rv; }
		
  m_atr.resize(dwAtrLen);
  m_atr.duplicate((const char*)pbAtr, dwAtrLen);

  return SCARD_S_SUCCESS;
}

/** No descriptions */
long CardHelper::disconnectCard(){
  long rv;

  if(!m_cardPresent) { return -1; }

  rv = SCardDisconnect( hCard, SCARD_UNPOWER_CARD );
  if(rv != SCARD_S_SUCCESS) { return rv; }

  return SCARD_S_SUCCESS;
}

long CardHelper::selectFile(QString fileID, QByteArray &header) {
  long rv;

  // Select file.
  QString apdu = "a4 a4 00 00 02 " + fileID;
  QString resp;
  rv = sendAPDU(apdu, resp);

  header = str2ba(resp);

  return rv;
}

QString _file_types[] = {
  "Transparent", "Linear Fixed", "Linear Variable", "Cyclic", "(Reserved)",
  "Reserved", "EF_ASC", "?", "?", "?", "?", "?", "?", "?", "?", "?",
  "DF (no ASC)", "DF with ASC", "DF, sub-DFs not allowed",
  "DF, sub-DFs allowed"
};

QString _access[] = {
  "ALWays", "CHV1", "CHV2", "PROtected", "AUThenticated", "ENCrypyted",
  "CHV1+PROtected", "CHV2+PROtected", "CHV1+AUThenticated",
  "CHV2+AUThenticated", "CHV1+ENCrypyted", "CHV2+ENCrypyted",
  "RFU", "RFU", "RFU", "NEVer"
};

QString CardHelper::describeHeader(const QByteArray& ba) {
  unsigned short fid, ftype, fsize, i;
  bool readable;
  QString str,s;

  fid = (ba[4] << 8) + ba[5];
  str += s.sprintf("File ID = %X<br>", fid);
  if((fid > 0xfd00) && (fid < 0xfd1f)) {
    str += s.sprintf("Short file identifier = %X<br>", fid & 0x001F);
  } else if((fid & 0xFF80) == 0xFF80) {
    str += s.sprintf("File for public key number %X<br>", fid & 0x007F);
  } else if((fid & 0xFF80) == 0xFF00) {
    str += s.sprintf("File for private key number %X<br>",fid & 0x007F);
  }

  ftype = ba[13];
  str += s.sprintf("File type = %s (%X)<br>", _file_types[ftype].latin1(),
		   ftype);

  if((ba.size() > 16) && ((ftype > 0) && (ftype < 4))) {
    str += s.sprintf("Record length = %i<br>", ba[14]);
  }

  fsize = (ba[2] << 8) + ba[3];
  str += s.sprintf("File size = %i (0x%X)<br>",fsize,fsize);

  i = (ba[8] >> 4) & 0xf;
  readable = (i == 0);

  str += s.sprintf("Access for Read = %s<br>", _access[i].latin1());
  i = ba[8] & 0xf;
  str += s.sprintf("Access for Update = %s<br>", _access[i].latin1());
  i = (ba[9]>>4) & 0xf;
  str += s.sprintf("Access for Delete group or Import Key = %s<br>", 
		   _access[i].latin1());
  i = ba[9] & 0xf;
  str += s.sprintf("Access for Create group or Use Key = %s<br>", 
		   _access[i].latin1());
  i = (ba[10]>>4) & 0xf;
  str += s.sprintf("Access for Rehabilitate = %s<br>", _access[i].latin1());
  i = ba[10] & 0xf;
  str += s.sprintf("Access for Invalidate = %s<br>", _access[i].latin1());
  i = ba[11];
  str += s.sprintf("File status = %X<br>", i);
  str += "File header:<br>";
  str += ba2str(ba) + "<br>";

  return str;
}

long CardHelper::beginTransaction() {
  long rv;

  rv = connectCard();
  if(rv != SCARD_S_SUCCESS) { return rv; }

  rv = SCardBeginTransaction( hCard );

  return rv;
}

long CardHelper::endTransaction() {
  long rv;

  rv = SCardEndTransaction( hCard, SCARD_UNPOWER_CARD );

  return rv;
}

/** No descriptions */
long CardHelper::readFile(QString& list){
  QByteArray ba;
  long rv;

  rv = readFile(ba);
  if(rv == SCARD_S_SUCCESS) {
    QString l = ba2str(ba);
    list += l;
  }

  return rv;
}

long CardHelper::readFile(QByteArray &ba){
  if(!m_isConnected || !m_cardPresent) { return SCARD_E_NO_SMARTCARD; }

  long rv;
  QString apdu, resp;

  apdu = "a4 b0 00 00";
  rv = sendAPDU(apdu, resp);
  if(rv != SCARD_S_SUCCESS) { return rv; }

  ba = str2ba(resp);
 
  return rv;
}

/** No descriptions */
QByteArray CardHelper::str2ba(QString str){
  QString hex;
  QByteArray ba;
  int i = 0;

  DEBUG_MSG("str2ba" << endl);

  ba.resize(str.length());
  str = str.stripWhiteSpace();
  while(str.length() >= 2) {
    hex = str.left(2);
    str = str.right(str.length() - 2);

    DEBUG_MSG(hex);
    ba[i++] = hex.toUInt(0, 16);
    str = str.stripWhiteSpace();
  }
  ba.resize(i);
  DEBUG_MSG(endl);

  return ba;
}

/** No descriptions */
QString CardHelper::ba2str(QByteArray ba){
  QStringList str_list;
  QString str;

  DEBUG_MSG("ba2str" << endl);

  for(unsigned int i=0; i<ba.size(); i++) {
    str.sprintf("%02X", (BYTE)ba[i]);
    DEBUG_MSG(str);
    str_list.append(str);
  }
  DEBUG_MSG(endl);

  return str_list.join(" ");
}

/** No descriptions */
long CardHelper::sendAPDU(const QByteArray& ba, QByteArray& ret){
  long rv;
  SCARD_IO_REQUEST pioSendPci;
  SCARD_IO_REQUEST pioRecvPci;
  DWORD pcbRecvLength = 255;
  BYTE pbRecvBuffer[255];

  pioSendPci.dwProtocol = m_prefProtocol;
  pioSendPci.cbPciLength = 8;
  DEBUG_MSG("sendAPDU: sending " << ba2str(ba) << endl);
  rv = SCardTransmit( hCard, &pioSendPci, (BYTE*)ba.data(),
            		      ba.size(), &pioRecvPci, pbRecvBuffer,
            		      &pcbRecvLength);

  ret.resize(pcbRecvLength);
  unsigned int i = pcbRecvLength;
  ret.duplicate((const char*)pbRecvBuffer, i);

  DEBUG_MSG("sendAPDU: received " << ba2str(ret) << endl);

  if(ret.size() < 2) { 
    m_apduReturn[0] = 0;
    m_apduReturn[1] = 0;
    return -1; 
  }

  m_apduReturn[0] = ret[ret.size()-2];
  m_apduReturn[1] = ret[ret.size()-1];
  ret.resize(ret.size()-2);

  return rv;
}

QString CardHelper::APDUReturnCode() {
  return ba2str(m_apduReturn);
}

/** No descriptions */
long CardHelper::sendAPDU(QString apdu, QString &resp){
  QByteArray rp;
  QByteArray send;
  long rv;

  send = str2ba(apdu);

  rv = sendAPDU(send, rp);
  if(rv == SCARD_S_SUCCESS) {
    resp = ba2str(rp);
  }

  return rv;
}

/** No descriptions */
QString CardHelper::ATR(){
  if(!m_isConnected || !m_cardPresent) {
    return QString();
  }

  return ba2str(m_atr);
}
