#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './main_win.ui'
**
** Created: Fri Jul 6 13:06:38 2001
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "main_win.h"

#include <qpushbutton.h>
#include <qtextview.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/* 
 *  Constructs a MainWindow which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 */
MainWindow::MainWindow( QWidget* parent,  const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "MainWindow" );
    resize( 541, 378 ); 
    setCaption( i18n( "Cambridge Card Viewer" ) );
    MainWindowLayout = new QVBoxLayout( this ); 
    MainWindowLayout->setSpacing( 6 );
    MainWindowLayout->setMargin( 11 );

    Layout1 = new QHBoxLayout; 
    Layout1->setSpacing( 6 );
    Layout1->setMargin( 0 );

    PushButton1 = new QPushButton( this, "PushButton1" );
    PushButton1->setText( i18n( "&View" ) );
    Layout1->addWidget( PushButton1 );

    PushButton1_2 = new QPushButton( this, "PushButton1_2" );
    PushButton1_2->setText( i18n( "&About..." ) );
    Layout1->addWidget( PushButton1_2 );

    PushButton1_2_2 = new QPushButton( this, "PushButton1_2_2" );
    PushButton1_2_2->setText( i18n( "E&xit" ) );
    Layout1->addWidget( PushButton1_2_2 );
    QSpacerItem* spacer = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( spacer );
    MainWindowLayout->addLayout( Layout1 );

    text_box = new QTextView( this, "text_box" );
    QFont text_box_font(  text_box->font() );
    text_box_font.setFamily( "Courier New" );
    text_box->setFont( text_box_font ); 
    text_box->setTextFormat( QTextView::PlainText );
    MainWindowLayout->addWidget( text_box );

    // signals and slots connections
    connect( PushButton1_2_2, SIGNAL( clicked() ), this, SLOT( slotExit() ) );
    connect( PushButton1_2, SIGNAL( clicked() ), this, SLOT( slotAbout() ) );
    connect( PushButton1, SIGNAL( clicked() ), this, SLOT( slotView() ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
MainWindow::~MainWindow()
{
    // no need to delete child widgets, Qt does it all for us
}

/*  
 *  Main event handler. Reimplemented to handle application
 *  font changes
 */
bool MainWindow::event( QEvent* ev )
{
    bool ret = QWidget::event( ev ); 
    if ( ev->type() == QEvent::ApplicationFontChange ) {
	QFont text_box_font(  text_box->font() );
	text_box_font.setFamily( "Courier New" );
	text_box->setFont( text_box_font ); 
    }
    return ret;
}

void MainWindow::slotView()
{
    qWarning( "MainWindow::slotView(): Not implemented yet!" );
}

void MainWindow::slotAbout()
{
    qWarning( "MainWindow::slotAbout(): Not implemented yet!" );
}

void MainWindow::slotExit()
{
    qWarning( "MainWindow::slotExit(): Not implemented yet!" );
}

#include "main_win.moc"
