/***************************************************************************
                          cardhelper.h  -  description
                             -------------------
    begin                : Fri Jul 6 2001
    copyright            : (C) 2001 by Richard Wareham
    email                : richwareham@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef CARDHELPER_H
#define CARDHELPER_H

#include <winscard.h>
#include <pcsclite.h>

#include <qstring.h>

#include <qstringlist.h>
#include <qcstring.h>

/**
 * @short Wrapper object to abstract the PC/SC API and access
 * the filesystem on the card.
 *
 * This class takes care of communicating to the PC/SC middleware
 * and accessing the features of the Cambridge University
 * smartcard. 
 *
 * The helper class requires that you explicitly connect to
 * the middleware and the card. For example, the following code
 * will print the contents of the summary file (FD 03) on the
 * card.
 *
 * <pre>
 * CardHelper helper;
 *  
 * helper.connect("localhost");
 * helper.connectCard();
 *   
 * cout << helper.readFileString("FD 03");
 *
 * helper.disconnectCard();
 * helper.disconnect();
 * </pre>
 *
 * @author Richard Wareham <richwareham@users.sourceforge.net>
 * @version 0.1alpha
 */

class CardHelper {
private:
  SCARDHANDLE hCard;
  SCARDCONTEXT hContext;

  bool m_isConnected;
  bool m_cardPresent;
  QString m_reader;
  unsigned long m_prefProtocol;
  QByteArray m_apduReturn;
  QByteArray m_atr;

  QStringList m_readers;
public: 
  /**
   * Default constructor.
   */
  CardHelper();
  /**
   * Default destructor.
   */
  ~CardHelper();

  /**
   * Connect to the PC/SC middleware. You must call this method before
   * doing anything else.
   *
   * @param host The hostname of the machine you wish to connect to. If
   * not specified, the PC/SC middleware on the local machine is contacted.
   * By default, the first reported smartcard reader is used.
   *
   * @see CardHelper#disconnect
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   */
  long connect(QString host = 0);
  /**
   * Disconnectes from the PC/SC middleware peviously connected to
   * with connect().
   *
   * @see CardHelper#connect
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   */
  long disconnect();
  /**
   * @return A QStringList containing a description of each smartcard
   * reader connected to the PC/SC middleware. These descriptions are
   * what you should pass to setReader() when changing readers.
   *
   * @see CardHelper#setReader
   */
  QStringList readers();
  /**
   * Set the smartcard reader which should be used. 
   *
   * @param reader One of the readers returned by readers().
   *
   * @see CardHelper#readers
   */
  void setReader(QString reader);
  /**
   * @return The smartcard reader which is being used.
   */
  QString reader();
  /**
   * Connects to the smartcard in the currently selected reader.
   *
   * @return SCARD_S_SUCCESS if successful, -1 if there is no card, 
   * an error code otherwise.
   *
   * @see CardHelper#disconnectCard
   */
  long connectCard();
  /**
   * Disconnects ftom a previously connected to card.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   *
   * @see CardHelper#connectCard
   */
  long disconnectCard();
  /**
   * Selects the file specified by fileID. This command should be
   * used before doing anything file-related. For example:
   *
   * <pre>
   * helper.beginTransaction();
   * helper.selectFile("FD 03");
   * helper.readFile(file);
   * helper.endTransaction();
   * </pre>
   *
   * @param fileID A string containing the fileID of the file to read
   * in hex. 
   *
   * @param ba A byte array to write the file header into.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   */
  long selectFile(QString fileID, QByteArray &ba);
  /**
   * @param ba A QByteArray containing
   * @return A description of the file header as returned by selectFile.
   *
   * @see CardHelper#selectFile
   */
  QString describeHeader(const QByteArray& ba);
  /**
   * Starts a transaction with the card. This must be called before
   * doing anthing with the card.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise. 
   */
  long beginTransaction();
  /**
   * Finishes a transaction with the card. This must be called after
   * doing anthing with the card.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   *
   * @see CardHelper#beginTransaction
   */
  long endTransaction();
  /**
   * Reads the file specified by fileID and appends a textual
   * representation of the file to a passed QString. For example
   * to append the contents of the summary file (0xFD03) to the
   * QString pased, use the following code:
   *
   * @param str The QString to append file contents to.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   */
  long readFile(QString &str);
  /** 
   * As above, but the file contents are assigned to a passed
   * QByteArray.
   * 
   * @param ba The QByteArray to append file contents to.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   */
  long readFile(QByteArray &ba);
  /**
   * Sends an APDU to the smartcard and writes the reply into
   * a QByteArray.
   *
   * @param ba A QByteArray which contains the APDU to send.
   * @param ret The QByteArray to write the response into.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   */
  long sendAPDU(const QByteArray& ba, QByteArray& ret);
  /**
   * As above except that the APDU and response are passed as
   * formatted strings (e.g. 0xFD03 => "FD 03")
   *
   * @param apdu The APDU to send to the card.
   *
   * @param resp The QString to write the response into.
   *
   * @return SCARD_S_SUCCESS if successful, an error code otherwise.
   */
  long sendAPDU(const QString apdu, QString& resp);
  /**
   * Get the return code from the last send APDU.
   *
   * @return A formatted hex string containing the return code. For
   * example, if the return code was 0x9000, the string returned
   * is "90 00".
   */
  QString APDUReturnCode();
  /**
   * @return A string containing the ATR from the smartcard.
   */
  QString ATR();
protected: // Protected methods
  /** No descriptions */
  QByteArray str2ba(QString str);
  /** No descriptions */
  QString ba2str(QByteArray ba);
};

#endif
