/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Fri Jul  6 09:00:44 BST 2001
    copyright            : (C) 2001 by Richard Wareham
    email                : richwareham@users.sourceforge.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <klocale.h>

#include "kcardview.h"

static const char *description =
	I18N_NOOP("KCardView dumps some info on a Cambridge Card.");
	
static KCmdLineOptions options[] =
{
  { 0, 0, 0 }
  // INSERT YOUR COMMANDLINE OPTIONS HERE
};

int main(int argc, char *argv[])
{

  KAboutData aboutData( "kcardview", I18N_NOOP("KCardView"),
    VERSION, description, KAboutData::License_GPL,
    "(c) 2001, Richard Wareham");
  aboutData.addAuthor("Richard Wareham",0, "richwareham@users.sourceforge.net");
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  KApplication a;
  KCardView *kcardview = new KCardView();
  a.setMainWidget(kcardview);
  kcardview->show();  

  return a.exec();
}
